package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingLot {

    private List<ParkingTicket> ticketList;
    private int remainingCapacity;

    public ParkingLot() {
        this.remainingCapacity = 10;
        this.ticketList = new ArrayList<>();
    }

    public ParkingLot(int remainingCapacity){
        this.remainingCapacity = remainingCapacity;
        this.ticketList = new ArrayList<>();
    }

    public ParkingTicket park(Car car) {
        if (remainingCapacity > 0){
            this.remainingCapacity -= 1;
            ParkingTicket parkingTicket = new ParkingTicket(car);
            ticketList.add(parkingTicket);
            return parkingTicket;
        } else
            throw new NoAvailablePosition();
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if (ticketList.contains(parkingTicket)){
            ticketList.remove(parkingTicket);
            remainingCapacity += 1;
            parkingTicket.setUsed(true);
            return parkingTicket.getCar();
        }else if (parkingTicket.isUsed()){
            throw new UnRecognizedParkingTicketException();
        }else {
            throw new UnRecognizedParkingTicketException();
        }
    }

    public List<ParkingTicket> getTicketList() {
        return ticketList;
    }

    public int getRemainingCapacity() {
        return remainingCapacity;
    }

    public void setRemainingCapacity(int remainingCapacity) {
        this.remainingCapacity = remainingCapacity;
    }
}
