package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {

    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car_and_parkingBoy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //then
        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //when
        Car fetchCar = parkingBoy.fetch(parkingTicket);
        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_throw_exception_message_with_error_when_park_given_full_parking_lot_and_car_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        parkingLot.setRemainingCapacity(1);
        Car car1 = new Car();
        parkingBoy.park(car1);
        Car car2 = new Car();
        //when
        //then
        var exception = assertThrows(NoAvailablePosition.class, () -> parkingBoy.park(car2));
        assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_return_two_right_car_when_fetch_given_parking_lot_and_two_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car coolCar = new Car();
        Car notCoolCar = new Car();
        ParkingTicket parkingTicket1 = parkingBoy.park(coolCar);
        ParkingTicket parkingTicket2 = parkingBoy.park(notCoolCar);
        //when
        Car fetchCoolCar = parkingBoy.fetch(parkingTicket1);
        Car fetchNotCoolCar = parkingBoy.fetch(parkingTicket2);
        //then
        assertEquals(coolCar, fetchCoolCar);
        assertEquals(notCoolCar, fetchNotCoolCar);
    }

    @Test
    void should_throw_exception_wtih_error_message_when_fetch_given_parking_lot_and_wrong_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket wrongTicket = new ParkingTicket(new Car());
        //when
        //then
        var exception = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(wrongTicket));
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_used_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket parkingTicket = parkingBoy.park(new Car());
        Car fetchCar = parkingBoy.fetch(parkingTicket);
        //when
        var exception = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(parkingTicket));
        //then
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }
}
